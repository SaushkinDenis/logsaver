package repository

import "context"

type PostStorer interface {
	Insert(ctx context.Context, post Post) error
	Update(ctx context.Context, post Post) error
	Delete(ctx context.Context, id string) error
	FindOne(ctx context.Context, id string) (Post, error)
}

type Post struct {
	ID          string `json:"id"`
	ServiceName string `json:"serviceName"`
	Message     string `json:"message"`
}
