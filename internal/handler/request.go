package handler

type createRequest struct {
	ServiceName string `json:"serviceName"`
	Message     string `json:"message"`
}

type updateRequest struct {
	ID          string
	ServiceName string `json:"serviceName"`
	Message     string `json:"message"`
}

type deleteRequest struct {
	ID string
}

type findRequest struct {
	ID string
}
