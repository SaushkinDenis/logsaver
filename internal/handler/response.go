package handler

type createResponse struct {
	ID string `json:"id"`
}

type findResponse struct {
	ID          string `json:"id"`
	ServiceName string `json:"serviceName"`
	Message     string `json:"message"`
}
