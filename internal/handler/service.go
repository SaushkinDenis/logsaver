package handler

import (
	"context"
	"github.com/google/uuid"
	service2 "logSaver/internal/repository"
)

type service struct {
	storage service2.PostStorer
}

func (s service) create(ctx context.Context, req createRequest) (createResponse, error) {
	id := uuid.New().String()

	doc := service2.Post{
		ID:          id,
		ServiceName: req.ServiceName,
		Message:     req.Message,
	}

	if err := s.storage.Insert(ctx, doc); err != nil {
		return createResponse{}, err
	}

	return createResponse{ID: id}, nil
}

func (s service) update(ctx context.Context, req updateRequest) error {
	doc := service2.Post{
		ID:          req.ID,
		ServiceName: req.ServiceName,
		Message:     req.Message,
	}

	if err := s.storage.Update(ctx, doc); err != nil {
		return err
	}

	return nil
}

func (s service) delete(ctx context.Context, req deleteRequest) error {
	if err := s.storage.Delete(ctx, req.ID); err != nil {
		return err
	}

	return nil
}

func (s service) find(ctx context.Context, req findRequest) (findResponse, error) {
	post, err := s.storage.FindOne(ctx, req.ID)
	if err != nil {
		return findResponse{}, err
	}

	return findResponse{
		ID:          post.ID,
		ServiceName: post.ServiceName,
		Message:     post.Message,
	}, nil
}
