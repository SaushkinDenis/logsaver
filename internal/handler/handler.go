package handler

import (
	"github.com/julienschmidt/httprouter"
	"logSaver/internal/repository"
)

type Handler struct {
	service service
}

func NewHandler(storage repository.PostStorer) Handler {
	return Handler{
		service: service{storage: storage},
	}
}

func (h Handler) InitRoutes() *httprouter.Router {
	router := httprouter.New()
	router.HandlerFunc("POST", "/api/v1/posts", h.Create)
	router.HandlerFunc("PATCH", "/api/v1/posts/:id", h.Update)
	router.HandlerFunc("DELETE", "/api/v1/posts/:id", h.Delete)
	router.HandlerFunc("GET", "/api/v1/posts/:id", h.Find)
	return router
}
