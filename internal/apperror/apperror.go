package apperror

import (
	"encoding/json"
	"errors"
	"fmt"
)

type AppError struct {
	Err     error  `json:"-"`
	Message string `json:"message,omitempty"`
	Code    int    `json:"code,omitempty"`
}

func (e *AppError) Error() string {
	return e.Err.Error()
}

func (e *AppError) Unwrap() error { return e.Err }

func (e *AppError) Marshal() []byte {
	marshal, err := json.Marshal(e)
	if err != nil {
		return nil
	}
	return marshal
}

func NewAppError(err error, message string, code int) *AppError {
	return &AppError{
		Err:     err,
		Message: message,
		Code:    code,
	}
}

func NewGoneError(msg string) error {
	return NewAppError(errors.New(msg), msg, 410)
}

func NewAccessDeniedError() error {
	return NewAppError(errors.New("У вас нет прав доступа"), "У вас нет прав доступа", 403)
}

func NewUnauthorizedError(err error) error {
	return NewAppError(err, "Не удалось авторизовать пользователя", 401)
}

// base
func NewInternalError(err error) error {
	return NewAppError(err, "Возникла непредвиденная ошибка, обратитесь в тех. поддержку", 500)
}

func NewBadRequestError(err error, message string) error {
	return NewAppError(err, message, 400)
}

func NewInvalidTokenError() error {
	return NewAppError(errors.New("Токен не валиден"), "Токен не валиден", 422)
}

func NewConflictError(message string) error {
	return NewAppError(errors.New(message), message, 409)
}

func NewInvalidDataError(message string) error {
	return NewAppError(errors.New(message), message, 422)
}

func NewServiceUnavailableError(err error, serviceName string) error {
	return NewAppError(err, fmt.Sprintf("Недоступен сервис %s", serviceName), 503)
}
