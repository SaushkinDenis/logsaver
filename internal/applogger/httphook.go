package applogger

import (
	"bytes"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"logSaver/pkg/constants"
	"net/http"
)

type HttpHook struct {
	initiatorName string
	url           string
	logChan       chan logrus.Entry
}

type HttpHookRequest struct {
	InitiatorName string `json:"initiatorName"`
	Msg           string `json:"msg"`
}

func (hook *HttpHook) Fire(entry *logrus.Entry) error {
	// send the log data to the channel
	hook.logChan <- *entry

	return nil
}

func (hook *HttpHook) Levels() []logrus.Level {
	return logrus.AllLevels
}

func (hook *HttpHook) Run() {
	logrus.Infof("Webhook start with address %s", hook.url)
	for {
		select {
		case entry := <-hook.logChan:
			msg, err := entry.String()
			if err != nil {
				logrus.Errorf("Failed to get string from logrus entry: %v", entry)
				continue
			}
			requestId := hook.getFieldFromEntry(constants.RequestIdCtx, &entry)
			req := HttpHookRequest{Msg: msg, InitiatorName: hook.initiatorName}
			body, err := json.Marshal(req)
			if err != nil {
				logrus.Errorf("Failed to marshal body: %v, requestId: %s", req, requestId)
				continue
			}
			resp, err := http.Post(hook.url, "application/json", bytes.NewReader(body))
			if err != nil {
				logrus.Errorf("Failed to send log message to HTTP endpoint: %v, requestId: %s", err, requestId)
				continue
			}

			if resp.StatusCode != http.StatusOK {
				logrus.Errorf("HTTP request failed with status %d, requestId: %s", resp.StatusCode, requestId)
			}
		}
	}
}

func (hook *HttpHook) getFieldFromEntry(key string, entry *logrus.Entry) string {
	for i, v := range entry.Data {
		if i == key {
			return v.(string)
		}
	}
	return ""
}
