package applogger

import (
	"context"
	"github.com/sirupsen/logrus"
	"log"
	"logSaver/pkg/constants"
	"os"
	"time"
)

var lgr *logrus.Logger

type logger struct {
	log *logrus.Logger
}

func NewLogger(level, url, initiator string, webHookEnabled bool) Logger {
	l := logrus.New()

	lvl, err := logrus.ParseLevel(level)
	if err != nil {
		log.Fatal(err)
	}
	l.SetLevel(lvl)
	l.Formatter = &logrus.JSONFormatter{}
	l.SetOutput(os.Stdout)

	if url != "" && webHookEnabled {
		hook := &HttpHook{url: url, initiatorName: initiator, logChan: make(chan logrus.Entry, 100)}
		l.AddHook(hook)
		go hook.Run()
	}

	lgr = l
	return &logger{
		log: l,
	}
}

func LoggerCtx(ctx context.Context) *logrus.Entry {
	entry := lgr.WithTime(time.Now().UTC())
	if ctx != nil {
		if ctxRqId, ok := ctx.Value(constants.RequestIdCtx).(string); ok {
			entry = entry.WithField(constants.RequestIdCtx, ctxRqId)
		}
		if userIdCtx, ok := ctx.Value(constants.UserIdCtx).(int64); ok {
			entry = entry.WithField(constants.UserIdCtx, userIdCtx)
		}
		if userRoleCtx, ok := ctx.Value(constants.UserRoleCtx).(string); ok {
			entry = entry.WithField(constants.UserRoleCtx, userRoleCtx)
		}
		if apiNameCtx, ok := ctx.Value(constants.ApiNameCtx).(string); ok {
			entry = entry.WithField(constants.ApiNameCtx, apiNameCtx)
		}
		if methodNameCtx, ok := ctx.Value(constants.MethodNameCtx).(string); ok {
			entry = entry.WithField(constants.MethodNameCtx, methodNameCtx)
		}
		if methodTypeCtx, ok := ctx.Value(constants.MethodTypeCtx).(string); ok {
			entry = entry.WithField(constants.MethodTypeCtx, methodTypeCtx)
		}
	}
	return entry
}

func (logger *logger) Debug(args ...interface{}) {
	logger.log.Debug(args)
}

func (logger *logger) Info(args ...interface{}) {
	logger.log.Info(args)
}

func (logger *logger) Warn(args ...interface{}) {
	logger.log.Warn(args)
}

func (logger *logger) Error(args ...interface{}) {
	logger.log.Error(args)
}

func (logger *logger) Tracef(format string, args ...interface{}) {
	logger.log.Tracef(format, args)
}

func (logger *logger) Debugf(format string, args ...interface{}) {
	logger.log.Debugf(format, args)
}

func (logger *logger) Infof(format string, args ...interface{}) {
	logger.log.Infof(format, args)
}

func (logger *logger) Warnf(format string, args ...interface{}) {
	logger.log.Warnf(format, args)
}

func (logger *logger) Errorf(format string, args ...interface{}) {
	logger.log.Errorf(format, args)
}

func (logger *logger) Fatalf(format string, args ...interface{}) {
	logger.log.Fatalf(format, args)
}
