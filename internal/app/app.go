package app

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"log"
	"logSaver"
	"logSaver/internal/handler"
	"logSaver/internal/repository/elasticsearch"
	"os"
	"os/signal"
	"syscall"
)

func Run() {
	elastic, err := elasticsearch.New([]string{viper.GetString("ElasticSearchURL")})
	if err != nil {
		log.Fatalln(err)
	}
	if err := elastic.CreateIndex("post"); err != nil {
		log.Fatalln(err)
	}

	// Start HTTP server.
	server := new(logSaver.Server)

	storage, err := elasticsearch.NewPostStorage(*elastic)
	if err != nil {
		log.Fatalln(err)
	}

	handlers := handler.NewHandler(storage)

	go func() {
		if serverError := server.Run(viper.GetString("port"), handlers.InitRoutes()); serverError != nil {
			logrus.Fatalf("error occured while running server: %s", serverError.Error())
		}
	}()

	logrus.Println("Started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Println("Shutting down")

	if err := server.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}
}
