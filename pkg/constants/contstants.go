package constants

// Headers
const (
	AuthorizationHeader = "Authorization"
	RequestIdHeader     = "X-REQUEST-ID"
	RefreshHeader       = "X-REFRESH-TOKEN"
)

// Context
const (
	CompanyIdCtx  = "companyId"
	UserIdCtx     = "userId"
	UserRoleCtx   = "userRole"
	IssuerCtx     = "issuer"
	JtiCtx        = "jti"
	ExpiredAtCtx  = "expiredAtCtx"
	RequestIdCtx  = "requestId"
	ApiNameCtx    = "apiName"
	MethodNameCtx = "methodName"
	MethodTypeCtx = "methodType"
)
