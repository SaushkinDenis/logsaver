package util

import (
	"logSaver/pkg/constants"
	"net/http"
	"strings"
)

var headerToMask = []string{constants.AuthorizationHeader}

// Mask works correct only for headers with a single header
func MaskHeaders(headers http.Header) http.Header {
	for _, v := range headerToMask {
		if h, ex := headers[v]; ex {
			headers.Set(v, maskBySize(headers.Get(h[0])))
		}
	}
	return headers
}

func maskBySize(val string) string {
	if len(val) < 1 {
		return ""
	} else if len(val) < 4 {
		return "***"
	} else if len(val) < 6 {
		return val[:3] + strings.Repeat("*", len(val)-3)
	} else if len(val) < 10 {
		return val[:4] + strings.Repeat("*", len(val)-4)
	} else if len(val) < 16 {
		return val[:5] + strings.Repeat("*", len(val)-5)
	}
	return val[:3] + "***" + val[len(val)-3:]
}
