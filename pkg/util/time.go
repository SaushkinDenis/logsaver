package util

import "time"

func GetCurrentMskTime() time.Time {
	return time.Now().UTC().Add(3 * time.Hour)
}

func GetCurrentUTCTime() time.Time {
	return time.Now().UTC().Add(3 * time.Hour)
}
