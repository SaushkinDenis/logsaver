package util

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"logSaver/pkg/constants"
	"time"
)

func GetUserId(c context.Context) (uuid.UUID, error) {
	id, err := getUUIDFromContext(c, constants.UserIdCtx)
	if err != nil {
		return [16]byte{}, err
	}
	return *id, nil
}

func GetCompanyId(c context.Context) (uuid.UUID, error) {
	id, err := getUUIDFromContext(c, constants.CompanyIdCtx)
	if err != nil {
		return [16]byte{}, err
	}
	return *id, nil
}

func GetIssuer(c context.Context) (string, error) {
	return getStringFromContext(c, constants.IssuerCtx)
}

func GetTokenId(c context.Context) (string, error) {
	return getStringFromContext(c, constants.JtiCtx)
}

func GetExpiredAt(c context.Context) (time.Time, error) {
	return getTimeFromContext(c, constants.ExpiredAtCtx)
}

func getTimeFromContext(c context.Context, context string) (time.Time, error) {
	val, ok := c.Value(context).(time.Time)
	if !ok {
		return time.Now(), errors.New(fmt.Sprintf("cant convert %s to time", context))
	}
	return val, nil
}

func getStringFromContext(c context.Context, context string) (string, error) {
	val, ok := c.Value(context).(string)
	if !ok {
		return "", errors.New(fmt.Sprintf("cant convert %s to string", context))
	}
	return val, nil
}

func getUUIDFromContext(c context.Context, context string) (*uuid.UUID, error) {
	idFromCtx, err := getStringFromContext(c, context)
	if err != nil {
		return nil, err
	}
	id, err := UUIDFromString(idFromCtx)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("cant deserialise id %s", context))
	}
	return &id, nil
}
