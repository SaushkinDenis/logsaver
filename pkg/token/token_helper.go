package token

import (
	"context"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"logSaver/internal/apperror"
	"logSaver/internal/applogger"
)

type CustomClaims struct {
	UserId    uuid.UUID
	CompanyId uuid.UUID
	Role      string
	jwt.StandardClaims
}

func ParseTokenWithoutKeyCheck(ctx context.Context, accessToken string) (*CustomClaims, error) {
	parser := jwt.Parser{}
	token, _, err := parser.ParseUnverified(accessToken, &CustomClaims{})
	if err != nil {
		applogger.LoggerCtx(ctx).Errorf("Cant parse token: %v", err)
		return nil, apperror.NewUnauthorizedError(err)
	}
	if err := token.Claims.Valid(); err != nil {
		applogger.LoggerCtx(ctx).Errorf("Claims is invalid: %v", err)
		return nil, apperror.NewUnauthorizedError(err)
	}
	claims, ok := token.Claims.(*CustomClaims)
	if !ok {
		applogger.LoggerCtx(ctx).Errorf("Cannot serialise claim to custom")
		return nil, apperror.NewUnauthorizedError(err)
	}
	return claims, nil
}

func GetUserRole(claims *CustomClaims) string {
	return claims.Role
}

func GetUserId(claims *CustomClaims) uuid.UUID {
	return claims.UserId
}

func GetCompanyId(claims *CustomClaims) uuid.UUID {
	return claims.CompanyId
}

func GetTokenId(claims *CustomClaims) string {
	return claims.Id
}
