package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/prometheus/client_golang/prometheus"
	"log"
	"time"
)

const (
	defaultMaxPoolSize  = 1
	defaultConnAttempts = 2
	defaultConnTimeout  = time.Second
	connMaxLifetime     = 3 * time.Second
)

// Postgres -.
type Postgres struct {
	maxPoolSize       int
	minPoolSize       int
	connMaxLifetime   time.Duration
	connMaxIdletime   time.Duration
	healthCheckPeriod time.Duration
	connAttempts      int
	connTimeout       time.Duration

	Pool *pgxpool.Pool
}

func GetCompleteUrl(userName, password, host, port, DBName, schema, sslMode, sslRootCert string) string {
	return fmt.Sprintf("postgresql://%s:%s@%s:%s/%s?search_path=%s",
		userName,
		password,
		host,
		port,
		DBName,
		schema)
}

func GetCompleteDsn(userName, password, host, port, DBName, schema, sslMode, sslRootCert string) string {
	str := fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s search_path=%s sslmode=%s sslrootcert=%s",
		userName,
		password,
		host,
		port,
		DBName,
		schema,
		sslMode,
		sslRootCert)
	return str
}

func New(url string, opts ...Option) (*Postgres, error) {
	pg := &Postgres{
		maxPoolSize:     defaultMaxPoolSize,
		connAttempts:    defaultConnAttempts,
		connTimeout:     defaultConnTimeout,
		connMaxLifetime: connMaxLifetime,
	}

	// Custom options
	for _, opt := range opts {
		opt(pg)
	}
	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil {
		log.Println(fmt.Errorf("postgres - NewPostgres - pgxpool.ParseConfig: %s", err))
		return nil, fmt.Errorf("postgres - NewPostgres - pgxpool.ParseConfig: %w", err)
	}
	poolConfig.MaxConns = int32(pg.maxPoolSize)
	poolConfig.MinConns = int32(pg.minPoolSize)
	poolConfig.MaxConnIdleTime = pg.connMaxIdletime
	poolConfig.MaxConnLifetime = pg.connMaxLifetime
	poolConfig.HealthCheckPeriod = pg.healthCheckPeriod

	/*logrusLogger := &logrus.Logger{
		Out:          os.Stderr,
		Formatter:    new(logrus.JSONFormatter),
		Hooks:        make(logrus.LevelHooks),
		Level:        logrus.InfoLevel,
		ExitFunc:     os.Exit,
		ReportCaller: false,
	}
	poolConfig.ConnConfig.Logger = logrusadapter.NewLogger(logrusLogger)*/
	for pg.connAttempts > 0 {
		pg.Pool, err = pgxpool.ConnectConfig(context.Background(), poolConfig)
		if err == nil {
			collector := NewCollector(pg.Pool, map[string]string{"db_name": "my_db"})
			prometheus.MustRegister(collector)
			break
		}

		log.Printf("Postgres is trying to connect, attempts left: %d", pg.connAttempts)

		time.Sleep(pg.connTimeout)

		pg.connAttempts--
	}

	if err != nil {
		return nil, fmt.Errorf("postgres - NewPostgres - connAttempts == 0: %w", err)
	}
	return pg, nil

}

// Close -.
func (p *Postgres) Close() {
	if p.Pool != nil {
		p.Pool.Close()
	}
}
