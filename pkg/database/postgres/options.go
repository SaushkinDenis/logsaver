package postgres

import "time"

// Option -.
type Option func(*Postgres)

// MaxPoolSize -.
func MaxPoolSize(size int) Option {
	return func(c *Postgres) {
		c.maxPoolSize = size
	}
}

// MaxPoolSize -.
func MinPoolSize(size int) Option {
	return func(c *Postgres) {
		c.minPoolSize = size
	}
}

// ConnMaxLifetime -.
func ConnMaxLifetime(maxLifetime time.Duration) Option {
	return func(c *Postgres) {
		c.connMaxLifetime = maxLifetime
	}
}

// ConnMaxIdletime -.
func ConnMaxIdletime(maxIdletime time.Duration) Option {
	return func(c *Postgres) {
		c.connMaxIdletime = maxIdletime
	}
}

// HealthCheckPeriod -.
func HealthCheckPeriod(healthCheckPeriod time.Duration) Option {
	return func(c *Postgres) {
		c.healthCheckPeriod = healthCheckPeriod
	}
}

// ConnAttempts -.
func ConnAttempts(attempts int) Option {
	return func(c *Postgres) {
		c.connAttempts = attempts
	}
}

// ConnTimeout -.
func ConnTimeout(timeout time.Duration) Option {
	return func(c *Postgres) {
		c.connTimeout = timeout
	}
}
