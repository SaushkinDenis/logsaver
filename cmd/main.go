package main

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"logSaver/internal/app"
)

func main() {
	// Configuration
	if err := initConfig(); err != nil {
		logrus.Fatalf("Error initialization config: %s", err.Error())
	}
	// Run
	app.Run()
}

func initConfig() error {
	viper.SetConfigName("config")
	viper.AddConfigPath("./configs")
	return viper.ReadInConfig()
}
